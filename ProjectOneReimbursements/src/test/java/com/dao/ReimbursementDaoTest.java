package com.dao;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.model.Reimbursement;
import com.model.User;

public class ReimbursementDaoTest {
	
	private static ReimbursementDao daoCall;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		daoCall = new ReimbursementDaoImpl("jdbc:h2:./testDBFolder/testData", "sa", "sa");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		daoCall.h2InitDao();
	}

	@After
	public void tearDown() throws Exception {
		daoCall.h2DestroyDao();
	}

	@Test
	public void selectReimbursementByIdTest() {
		int id = 1;
		Reimbursement reimb = daoCall.selectReimbursementById(id);
		System.out.println(reimb.getReimbAmount());
		assertEquals(1000.0, reimb.getReimbAmount(), 0);
		
		id = 2;
		reimb = daoCall.selectReimbursementById(id);
		assertEquals(2000.0, reimb.getReimbAmount(), 0);
	}
	
	@Test
	public void selectAllReimbursementsTest() {
		List<Reimbursement> reimbList = daoCall.selectAllReimbursements();
		
		Reimbursement tempReimb = reimbList.get(0);
		assertEquals(1000.0, tempReimb.getReimbAmount(), 0);
		
		tempReimb = reimbList.get(1);
		assertEquals(2000.0, tempReimb.getReimbAmount(), 0);
	}
	
	@Test
	public void createReimbursementTest() {
		Reimbursement reimb = new Reimbursement(0, 2600, "", "", "Gimme my money!", "DEADBEEF", 1, 0, "", "TRAVEL");
		daoCall.createReimbursement(reimb);
		Reimbursement testReimb = daoCall.selectReimbursementById(3);
		assertEquals(2600.0, testReimb.getReimbAmount(), 0);
	}
	
	@Test
	public void updateReimbursementTest() {
		User testManager = new User();
		testManager.setUserId(1);
		Reimbursement reimb = daoCall.selectReimbursementById(1);
		assertEquals("Testing before update", "approved", reimb.getReimbStatus());
		System.out.println(reimb);
		reimb.setReimbStatus("denied");
		daoCall.updateReimbursement(reimb, testManager);
		reimb = daoCall.selectReimbursementById(1);
		assertEquals("Testing after update", "denied", reimb.getReimbStatus());
	}
	
	@Test
	public void selectUserReimbursementsTest() {
		List<Reimbursement> reimbList = daoCall.selectUserReimbursements(1);
		Reimbursement testReimb = reimbList.get(0);
		assertEquals("correct reimbursement for user test", 1, testReimb.getReimbAuthor());
		assertEquals(1000.0, testReimb.getReimbAmount(), 0);
	}

}
