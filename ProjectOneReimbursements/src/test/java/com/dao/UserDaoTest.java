package com.dao;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.model.User;

public class UserDaoTest {
	
	private static UserDao daoCall;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		daoCall = new UserDaoImpl("jdbc:h2:./testDBFolder/testData", "sa", "sa");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		daoCall.h2InitDao();
	}

	@After
	public void tearDown() throws Exception {
		daoCall.h2DestroyDao();
	}

	@Test
	public void selectUserByIdTest() {
		int id = 1;
		User user = daoCall.selectUserById(id);
		assertEquals("Testing correct account", "ram", user.getUsername());
	}
	
	@Test
	public void selectUserByUsernameTest() {
		String username = "bob";
		User user = daoCall.selectUserByUsername(username);
		assertEquals("Testing id correct user was selected", "Bobby", user.getFirstName());
	}
	
}
