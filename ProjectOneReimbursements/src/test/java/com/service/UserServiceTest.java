package com.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.dao.UserDao;
import com.dao.UserDaoImpl;
import com.model.User;

public class UserServiceTest {
	private static UserService userServiceCall;
	private static UserDao userDaoCall;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		userServiceCall = new UserServiceImpl(true);
		userDaoCall = new UserDaoImpl("jdbc:h2:./testDBFolder/testData", "sa", "sa");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		userDaoCall.h2InitDao();
	}

	@After
	public void tearDown() throws Exception {
		userDaoCall.h2DestroyDao();
	}

	@Test
	public void getUserByIdTest() {
		int id = 1;
		User user = userServiceCall.getUserById(id);
		assertEquals("Correct user was gotten by service layer", "ram", user.getUsername());
		
	}
	
	@Test
	public void getUserByUsernameTest() {
		String username = "ram";
		User user = userServiceCall.getUserByUsername(username);
		assertEquals("correct user was selected", "Andrew", user.getFirstName());
	}
	
	@Test
	public void getAllUsersTest() {
		List<User> userList = userServiceCall.getAllUsers();
		User user = userList.get(0);
		assertEquals("Correct user was gotten by service layer", "ram", user.getUsername());
		user = userList.get(1);
		assertEquals("Correct user was gotten by service layer", "bob", user.getUsername());
	}
	
	@Test
	public void checkCredentialsTest() {
		String username = "ram";
		String password = "wrong password";
		User user = userServiceCall.checkCredentials(username, password);
		assertNull(user);
		password = "1234";
		user = userServiceCall.checkCredentials(username, password);
		assertNotNull(user);
	}

}
