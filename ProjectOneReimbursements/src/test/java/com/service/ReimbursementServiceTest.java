package com.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.dao.ReimbursementDao;
import com.dao.ReimbursementDaoImpl;
import com.model.Reimbursement;
import com.model.User;

public class ReimbursementServiceTest {

	private static ReimbursementService reimbServiceCall;
	private static ReimbursementDao reimbDaoCall;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		reimbServiceCall = new ReimbursementServiceImpl(true);
		reimbDaoCall = new ReimbursementDaoImpl("jdbc:h2:./testDBFolder/testData", "sa", "sa");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		reimbDaoCall.h2InitDao();
	}

	@After
	public void tearDown() throws Exception {
		reimbDaoCall.h2DestroyDao();
	}

	@Test
	public void getUserReimbursementsTest() {
		List<Reimbursement> reimbList = reimbServiceCall.getUserReimbursements(1);
		assertNotNull(reimbList);
		Reimbursement reimb = reimbList.get(0);
		assertEquals(1000.0, reimb.getReimbAmount(), 0);
	}
	
	@Test
	public void getAllReimbursementsTest() {
		List<Reimbursement> reimbList = reimbServiceCall.getAllReimbursements();
		assertNotNull(reimbList);
		Reimbursement reimb = reimbList.get(0);
		assertEquals(1000.0, reimb.getReimbAmount(), 0);
	}

	@Test
	public void getReimbursementByIdTest() {
		Reimbursement reimb = reimbServiceCall.getReimbursementById(1);
		assertNotNull(reimb);
		assertEquals(1000.0, reimb.getReimbAmount(), 0);
		
	}
	
	@Test
	public void insertReimbursementTest() {
		Reimbursement reimb = new Reimbursement(0, 2600, "", "", "Gimme my money!", "DEADBEEF", 1, 0, "", "TRAVEL");
		reimbServiceCall.insertReimbursement(reimb);
		Reimbursement testReimb = reimbServiceCall.getReimbursementById(3);
		assertEquals(2600.0, testReimb.getReimbAmount(), 0);
	}
	
	@Test 
	public void updateReimbursementTest() {
		User testManager = new User();
		testManager.setUserId(1);
		Reimbursement reimb = reimbServiceCall.getReimbursementById(1);
		assertEquals("Testing before update", "approved", reimb.getReimbStatus());
		System.out.println(reimb);
		reimb.setReimbStatus("denied");
		reimbServiceCall.updateReimbursement(reimb, testManager);
		reimb = reimbServiceCall.getReimbursementById(1);
		assertEquals("Testing after update", "denied", reimb.getReimbStatus());
	}
}
