package com.service;

import java.util.List;

import com.model.User;

public interface UserService {

	public List<User> getAllUsers();
	public User getUserById(int id);
	public User getUserByUsername(String username);
	public User checkCredentials(String username, String password);
	
}
