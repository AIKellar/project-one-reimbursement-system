package com.service;

import java.util.List;

import com.dao.ReimbursementDao;
import com.dao.ReimbursementDaoImpl;
import com.model.Reimbursement;
import com.model.User;

public class ReimbursementServiceImpl implements ReimbursementService {

	ReimbursementDao reimbDaoCall = new ReimbursementDaoImpl();
	public ReimbursementServiceImpl() {
		reimbDaoCall = new ReimbursementDaoImpl();
	}
	public ReimbursementServiceImpl(boolean h2Database) {
		reimbDaoCall = new ReimbursementDaoImpl("jdbc:h2:./testDBFolder/testData", "sa", "sa");
	}

	@Override
	public void insertReimbursement(Reimbursement reimb) {
		reimbDaoCall.createReimbursement(reimb);
	}

	@Override
	public Reimbursement updateReimbursement(Reimbursement reimb, User financeM) {
		reimbDaoCall.updateReimbursement(reimb, financeM);
		Reimbursement updatedReimb = reimbDaoCall.selectReimbursementById(reimb.getReimbId());
		return updatedReimb;
	}

	@Override
	public Reimbursement getReimbursementById(int reimbId) {
		Reimbursement reimb = reimbDaoCall.selectReimbursementById(reimbId);
		return reimb;
	}

	@Override
	public List<Reimbursement> getAllReimbursements() {
		List<Reimbursement> reimbList = reimbDaoCall.selectAllReimbursements();
		return reimbList;
	}

	@Override
	public List<Reimbursement> getUserReimbursements(int userId) {
		List<Reimbursement> reimbList = reimbDaoCall.selectUserReimbursements(userId);
		return reimbList;
	}

}
