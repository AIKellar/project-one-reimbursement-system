package com.service;

import java.util.List;

import com.model.Reimbursement;
import com.model.User;

public interface ReimbursementService {

	public void insertReimbursement(Reimbursement reimb);
	public Reimbursement updateReimbursement(Reimbursement reimb, User financeM);
	public Reimbursement getReimbursementById(int reimbId);
	public List<Reimbursement> getAllReimbursements();
	public List<Reimbursement> getUserReimbursements(int userId);
	
}
