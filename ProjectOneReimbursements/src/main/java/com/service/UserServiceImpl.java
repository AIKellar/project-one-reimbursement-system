package com.service;

import java.util.List;

import com.dao.UserDaoImpl;
import com.model.User;

public class UserServiceImpl implements UserService {
	UserDaoImpl userDaoCall = new UserDaoImpl();
	public UserServiceImpl() {
		userDaoCall = new UserDaoImpl();
	}
	public UserServiceImpl(boolean h2Database) {
		if (h2Database)
			userDaoCall = new UserDaoImpl("jdbc:h2:./testDBFolder/testData", "sa", "sa");
	}
	
	@Override
	public User getUserById(int id) {
		User user = userDaoCall.selectUserById(id);
		return user;
	}

	@Override
	public User getUserByUsername(String username) {
		User user = userDaoCall.selectUserByUsername(username);
		return user;
	}

	@Override
	public User checkCredentials(String username, String password) {
		User user = userDaoCall.selectUserByUsername(username);
		if(user == null)
			return null;
		if(password.equals(user.getPassword())) 
			return user;
		else
			return null;
	}

	@Override
	public List<User> getAllUsers() {
		List<User> userList = userDaoCall.selectAllUsers();
		return userList;
	}

}
