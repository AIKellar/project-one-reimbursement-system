package com.dao;

import java.util.List;

import com.model.Reimbursement;
import com.model.User;

public interface ReimbursementDao {

	public void createReimbursement(Reimbursement reimb);
	public Reimbursement updateReimbursement(Reimbursement reimb, User financeM);
	public Reimbursement selectReimbursementById(int reimbId);
	public List<Reimbursement> selectAllReimbursements();
	public List<Reimbursement> selectUserReimbursements(int userId);
	
	//H2 DATABASE FUNCTIONALITY
	public void h2InitDao();
	public void h2DestroyDao();

	
}
