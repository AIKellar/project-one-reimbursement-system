package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.model.User;

public class UserDaoImpl implements UserDao {
	
	final static Logger log = Logger.getLogger(UserDaoImpl.class);
	
	public UserDaoImpl() {
		// TODO Auto-generated constructor stub
	}
	public UserDaoImpl(String url, String username, String password) {
		this.url = url;
		this.username = username;
		this.password = password;
	}
	static {
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
    }
	
	private String url = "";
	private String username = "";
	private String password = "";

	@Override
	public User selectUserByUsername(String userName) {
		try( Connection conn = DriverManager.getConnection(url, username, password)) {
			
			String sql = "SELECT * FROM ers_users WHERE ers_username = ?;";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, userName);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				User user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
				int roleId = rs.getInt(7);
				switch(roleId) {
				case 1: {
					user.setRole("employee");
					break;
				}
				case 2: {
					user.setRole("finance manager");
					break;
				}
				}
				return user;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User selectUserById(int userId) {
		
try( Connection conn = DriverManager.getConnection(url, username, password)) {
			
			String sql = "SELECT * FROM ers_users WHERE ers_user_id = ?;";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				User user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
				int roleId = rs.getInt(7);
				switch(roleId) {
				case 1: {
					user.setRole("employee");
					break;
				}
				case 2: {
					user.setRole("finance Manager");
					break;
				}
				}
				return user;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	public List<User> selectAllUsers() {
		List<User> userList = new ArrayList<>();
try( Connection conn = DriverManager.getConnection(url, username, password)) {
			
			String sql = "SELECT * FROM ers_users;";
			
			PreparedStatement ps = conn.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				User user = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
				int roleId = rs.getInt(7);
				switch(roleId) {
				case 1: {
					user.setRole("employee");
					break;
				}
				case 2: {
					user.setRole("finance Manager");
					break;
				}
				}
				
				userList.add(user);
			}
			return userList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public void h2InitDao() {
		try (Connection conn = DriverManager.getConnection(url, this.username, password)) {
			String sql = "CREATE TABLE ers_users (\r\n" + 
					"	ers_user_id SERIAL PRIMARY KEY,\r\n" + 
					"	ers_username VARCHAR(50) UNIQUE,\r\n" + 
					"	ers_password VARCHAR(50),\r\n" + 
					"	user_first_name VARCHAR(100),\r\n" + 
					"	user_last_name VARCHAR(100),\r\n" + 
					"	user_email VARCHAR(150) UNIQUE,\r\n" + 
					"	user_role_id INTEGER\r\n" + 
					");" +
					"insert into ers_users values(default, 'ram', '1234', 'Andrew', 'Kellar', 'aikellar@aol.com', 1);\r\n" + 
					"insert into ers_users values(default, 'bob', '123', 'Bobby', 'Hill', 'bobby@aol.com', 2);";

			Statement state = conn.createStatement();
			state.execute(sql);

		} catch (SQLException e) {
			log.setLevel(Level.ERROR);
			log.error("Something happened on the back end, check sql statements", e);
		}
		
	}

	@Override
	public void h2DestroyDao() {
		try (Connection conn = DriverManager.getConnection(url, this.username, password)) {
			String sql = "DROP TABLE ers_users";
			
			Statement state = conn.createStatement();
			state.execute(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
