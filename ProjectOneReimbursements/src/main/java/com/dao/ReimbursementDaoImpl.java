package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.model.Reimbursement;
import com.model.User;

public class ReimbursementDaoImpl implements ReimbursementDao {
	final static Logger log = Logger.getLogger(ReimbursementDaoImpl.class);
	
	public ReimbursementDaoImpl() {
		// TODO Auto-generated constructor stub
	}
	public ReimbursementDaoImpl(String url, String username, String password) {
		this.url = url;
		this.username = username;
		this.password = password;
	}
	static {
        try {
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("Static block has failed me");
        }
    }
	
	private String url = "";
	private String username = "";
	private String password = "";

	@Override
	public void createReimbursement(Reimbursement reimb) {
		try( Connection conn = DriverManager.getConnection(url, username, password)) {
			
			String sql = "insert into ers_reimbursement values(default, ?, current_timestamp, null, ?, null, ?, null, 1, ?);";
			PreparedStatement ps = conn.prepareStatement(sql);
			int statusId=0;
			int reimbType=0;
			switch(reimb.getReimbStatus()) {
			case "pending": {
				statusId = 1;
				break;
			}
			case "approved": {
				statusId = 2;
				break;
			}
			case "denied": {
				statusId = 3;
				break;
			}
			}
			switch(reimb.getReimbType()) {
			case "LODGING": {
				reimbType=1;
				break;
			}
			case "TRAVEL": {
				reimbType=2;
				break;
			}
			case "FOOD": {
				reimbType=3;
				break;
			}
			case "OTHER": {
				reimbType=4;
				break;
			}
			}
			ps.setDouble(1, reimb.getReimbAmount());
			ps.setString(2, reimb.getReimbDescription());
			//ps.setString(3, reimb.getReimbReceipt());
			ps.setInt(3, reimb.getReimbAuthor());
			ps.setInt(4, reimbType);
			ps.executeUpdate();
			
			log.info("Reimbursement was created: " + reimb);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

	@Override
	public Reimbursement updateReimbursement(Reimbursement reimb, User financeM) {
		System.out.println(reimb.getReimbStatus());
		try( Connection conn = DriverManager.getConnection(url, username, password)) {
			String sql = "UPDATE ers_reimbursement set reimb_resolved = current_timestamp, reimb_resolver = ?, reimb_status_id = ?, reimb_type_id = ? where reimb_id = ?;";
			PreparedStatement ps = conn.prepareStatement(sql);
			int statusId = 0;
			int typeId = 0;
			switch(reimb.getReimbStatus()) {
			case "pending" : {
				statusId = 1; 
				break;
			}
			case "approved" : {
				statusId = 2;
				break;
			}
			case "denied" : {
				statusId = 3;
				break;
			}
			}
			switch(reimb.getReimbType()) {
			case "lodging" : {
				typeId = 1; 
				break;
			}
			case "travel" : {
				typeId = 2;
				break;
			}
			case "food" : {
				typeId = 3;
				break;
			}
			case "other" : {
				typeId = 4;
				break;
			}
			}
			System.out.println(statusId);
			ps.setInt(1, reimb.getReimbResolver());
			ps.setInt(2, statusId);
			ps.setInt(3, typeId);
			ps.setInt(4, reimb.getReimbId());
			ps.executeUpdate();
			
			log.info("Reimbursement was updated" + reimb);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReimbursementDao reimbDaoCall = new ReimbursementDaoImpl();
		reimb = reimbDaoCall.selectReimbursementById(reimb.getReimbId()); 
		return reimb;
	}

	@Override
	public Reimbursement selectReimbursementById(int reimbId) {
		try( Connection conn = DriverManager.getConnection(url, username, password)) {
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_id = ?;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, reimbId);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Reimbursement reimb = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8));
				switch(rs.getInt(9)) {
				case 1: {
					reimb.setReimbStatus("pending");
					break;
				}
				case 2: {
					reimb.setReimbStatus("approved");
					break;
				}
				case 3: {
					reimb.setReimbStatus("denied");
					break;
				}
				}
				switch(rs.getInt(10)) {
				case 1: {
					reimb.setReimbType("LODGING");
					break;
				}
				case 2: {
					reimb.setReimbType("TRAVEL");
					break;
				}
				case 3: {
					reimb.setReimbType("FOOD");
					break;
				}
				case 4: {
					reimb.setReimbType("OTHER");
					break;
				}
				}
				return reimb;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Reimbursement> selectAllReimbursements() {
		try( Connection conn = DriverManager.getConnection(url, username, password)) {
			String sql = "select * from ers_reimbursement;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();		
			List<Reimbursement> reimbList = new ArrayList<>();
			while(rs.next()) {
				Reimbursement reimb = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8));
				switch(rs.getInt(9)) {
				case 1: {
					reimb.setReimbStatus("pending");
					break;
				}
				case 2: {
					reimb.setReimbStatus("approved");
					break;
				}
				case 3: {
					reimb.setReimbStatus("denied");
					break;
				}
				}
				switch(rs.getInt(10)) {
				case 1: {
					reimb.setReimbType("LODGING");
					break;
				}
				case 2: {
					reimb.setReimbType("TRAVEL");
					break;
				}
				case 3: {
					reimb.setReimbType("FOOD");
					break;
				}
				case 4: {
					reimb.setReimbType("OTHER");
					break;
				}
				}
				reimbList.add(reimb);
			}
			return reimbList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Reimbursement> selectUserReimbursements(int userId) {
		try( Connection conn = DriverManager.getConnection(url, username, password)) {
			String sql = "SELECT * FROM ers_reimbursement WHERE reimb_author = ?;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, userId);
			
			ResultSet rs = ps.executeQuery();		
			List<Reimbursement> reimbList = new ArrayList<>();
			while(rs.next()) {
				Reimbursement reimb = new Reimbursement(rs.getInt(1), rs.getDouble(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8));
				switch(rs.getInt(9)) {
				case 1: {
					reimb.setReimbStatus("pending");
					break;
				}
				case 2: {
					reimb.setReimbStatus("approved");
					break;
				}
				case 3: {
					reimb.setReimbStatus("denied");
					break;
				}
				}
				switch(rs.getInt(10)) {
				case 1: {
					reimb.setReimbType("LODGING");
					break;
				}
				case 2: {
					reimb.setReimbType("TRAVEL");
					break;
				}
				case 3: {
					reimb.setReimbType("FOOD");
					break;
				}
				case 4: {
					reimb.setReimbType("OTHER");
					break;
				}
				}
				reimbList.add(reimb);
			}
			return reimbList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void h2InitDao() {
		try( Connection conn = DriverManager.getConnection(url, username, password)) {
			String sql = "CREATE TABLE ers_reimbursement (\r\n" + 
					"	reimb_id SERIAL PRIMARY KEY,\r\n" + 
					"	reimb_amount NUMERIC(7,2) NOT NULL,\r\n" + 
					"	reimb_submitted TIMESTAMP NOT NULL,\r\n" + 
					"	reimb_resolved TIMESTAMP,\r\n" + 
					"	reimb_description VARCHAR,\r\n" + 
					"	reimb_receipt bytea,\r\n" + 
					"	reimb_author INTEGER NOT NULL,\r\n" + 
					"	reimb_resolver INTEGER,\r\n" + 
					"	reimb_status_id INTEGER NOT NULL,\r\n" + 
					"	reimb_type_id INTEGER NOT NULL\r\n" + 
					");" + "insert into ers_reimbursement values(default, 1000, current_timestamp, current_timestamp, 'I need money and I need it now!', 'DEADBEEF', 1, 2, 2, 1);\r\n" + 
							"insert into ers_reimbursement values(default, 2000, current_timestamp, null, 'Weekend company trip', 'DEADBEEF', 1, 2, 2, 1);";
			Statement state = conn.createStatement();
			state.execute(sql);
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void h2DestroyDao() {
		try (Connection conn = DriverManager.getConnection(url, this.username, password)) {
			String sql = "DROP TABLE ers_reimbursement";
			
			Statement state = conn.createStatement();
			state.execute(sql);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}

}
