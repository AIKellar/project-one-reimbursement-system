package com.dao;

import com.model.User;

public interface UserDao {
	
	public User selectUserByUsername(String username);
	public User selectUserById(int userId);
	//H2 DATABASE FUNCTIONALITY
			public void h2InitDao();
			public void h2DestroyDao();

}
