package com.servlet;
import java.io.IOException;

//comment from notepad++
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.controller.HomeController;
import com.controller.LoginController;
import com.controller.ReimbursementController;
import com.fasterxml.jackson.core.JsonProcessingException;

public class ForwardingRequestHelper {

	public static String process(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		System.out.println("\t\tIn RequestHelper");
		System.out.println(request.getRequestURI());
		
		
		switch(request.getRequestURI()) {
		case "/ProjectOneReimbursements/forwarding/login":
			System.out.println("case1");
			return LoginController.login(request);
		case "/ProjectOneReimbursements/forwarding/home":
			System.out.println("case 2");
			return HomeController.home(request);
		case "/ProjectOneReimbursements/forwarding/create-reimbursement":
			System.out.println("case 3");
			return HomeController.createReimbursement(request);
		case "/ProjectOneReimbursements/forwarding/submit-reimbursement":
			System.out.println("case 4");
			return ReimbursementController.submitReimbursement(request);
		case "/ProjectOneReimbursements/forwarding/reimbursement":
			System.out.println("In forwarding/reimbursement");
			return HomeController.viewReimbursement(request, response);
		case "/ProjectOneReimbursements/forwarding/update-reimbursement":
			System.out.println("In update reimb");
			return ReimbursementController.updateReimbursement(request);
		case "/ProjectOneReimbursements/forwarding/logout":
			System.out.println("In logout");
			return LoginController.logout(request);
			default:
				System.out.println("bad checkpoint");
				return "/";
		}
	}

}
