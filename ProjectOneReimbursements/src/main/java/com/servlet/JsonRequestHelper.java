package com.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.controller.ReimbursementController;
import com.controller.UserController;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonRequestHelper {

	public static void process(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		System.out.println("json request uri: " +request.getRequestURI());
		
		switch(request.getRequestURI()) {
		case "/ProjectOneReimbursements/json/allReimbursements":
			ReimbursementController.allFinder(request, response);
			break;
		case "/ProjectOneReimbursements/json/user":
			System.out.println("in json/user");
			UserController.userFinder(request, response);
			break;
		case "/ProjectOneReimbursements/json/userReimbursements":
			System.out.println("in json/user reimbursements");
			ReimbursementController.userFinder(request, response);
			break;
		case "/ProjectOneReimbursements/json/reimbursements":
			System.out.println("in json/reimbursements");
			ReimbursementController.allFinder(request, response);
			break;
		case "/ProjectOneReimbursements/json/reimbursement":
			System.out.println("in json/reimbursement");
			ReimbursementController.viewReimbursement(request, response);
			break;
		case "/ProjectOneReimbursements/json/all-users":
			System.out.println("in all users request helper");
			UserController.allFinder(request, response);
			break;
		case "/ProjectOneReimbursements/json/logout":
			UserController.logout(request);
			break;
		case "/ProjectOneReimbursements/json/check-user-create-reimbursement":
			System.out.println("in json check user");
			UserController.checkUser(request, response);
			break;
			default:
				System.out.println("issue!!!");				
				response.getWriter().println("null");
				
		}
		
	}


}
