package com.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;

public class UserController {

	public static void userFinder(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		User user = (User) request.getSession().getAttribute("loggeduser");
		System.out.println(user);
		response.getWriter().write(new ObjectMapper().writeValueAsString(user));
	}
	
	public static void allFinder(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		UserService userServiceCall = new UserServiceImpl();
		
		List<User> userList = userServiceCall.getAllUsers();
		
		response.getWriter().write(new ObjectMapper().writeValueAsString(userList));
		
	}

	public static void logout(HttpServletRequest request) {
		System.out.println("session invalidated!");
		request.getSession().removeAttribute("loggeduser");
		request.getSession().invalidate();
		
	}

	public static void checkUser(HttpServletRequest request,  HttpServletResponse response) throws JsonProcessingException, IOException{
		User user = (User) request.getSession().getAttribute("loggeduser");
		System.out.println("user is: " + user);
		if(user.getRole() != "employee")
			user = null;
		response.getWriter().write(new ObjectMapper().writeValueAsString(user));
	}

}
