package com.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.model.Reimbursement;
import com.model.User;
import com.service.ReimbursementService;
import com.service.ReimbursementServiceImpl;

public class HomeController {

	public static String home(HttpServletRequest request) {
		User user = (User) request.getSession().getAttribute("loggeduser");
		if(user == null) 
			return "/";
		
		return "/resources/html/home.html";
	}

	public static String createReimbursement(HttpServletRequest request) {
		

			return "/resources/html/reimbursement.html";
	}

	public static String viewReimbursement(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		User user = (User) request.getSession().getAttribute("loggeduser");
		if(user.getRole() != "finance manager")
			return "/";
		
		ReimbursementService reimbServiceCall = new ReimbursementServiceImpl();

		int reimbId = Integer.parseInt(request.getParameter("id"));
		System.out.println(reimbId);
		
		
		Reimbursement reimb = reimbServiceCall.getReimbursementById(reimbId);
		request.getSession().setAttribute("reimb", reimb);
		System.out.println(reimb);
		
		return "/resources/html/update-reimbursement.html";
	}
	

}