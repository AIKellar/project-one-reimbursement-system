package com.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Reimbursement;
import com.model.User;
import com.service.ReimbursementService;
import com.service.ReimbursementServiceImpl;

public class ReimbursementController {
	
	

	public static void allFinder(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		ReimbursementService reimbServiceCall = new ReimbursementServiceImpl();
		
		List<Reimbursement> reimbList = reimbServiceCall.getAllReimbursements();
		
		response.getWriter().write(new ObjectMapper().writeValueAsString(reimbList));
		
	}

	public static void userFinder(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException, IOException {
		ReimbursementService reimbServiceCall = new ReimbursementServiceImpl();
		
		//need to grab user from session, and then grab reimbursements from that user. will need to make service and dao methods for this as well.
		User user = (User) request.getSession().getAttribute("loggeduser");
		if(user == null) {
			PrintWriter printer = response.getWriter();
			printer.write("No one is logged in!");
			return;
		}
		List<Reimbursement> reimbList = reimbServiceCall.getUserReimbursements(user.getUserId());
		
		response.getWriter().write(new ObjectMapper().writeValueAsString(reimbList));
	}

	public static String submitReimbursement(HttpServletRequest request) {
		ReimbursementService reimbServiceCall = new ReimbursementServiceImpl();
		
		User user = (User) request.getSession().getAttribute("loggeduser");
		if(user == null)
			return "http://localhost:9002/ProjectOneReimbursements/";
		else if(user.getRole() != "employee")
			return "/resources/html/home.html";
		int userId = user.getUserId();
		double reimbAmount = Double.parseDouble(request.getParameter("reimbAmount"));
		String reimbDescription = request.getParameter("reimbDescription");
		String reimbType = request.getParameter("reimbType");
		
		Reimbursement reimb = new Reimbursement(0, reimbAmount, "", "", reimbDescription, "null", userId, 0, "", reimbType);
		reimbServiceCall.insertReimbursement(reimb);
		return "/resources/html/home.html";
	}

	public static void viewReimbursement(HttpServletRequest request,  HttpServletResponse response) throws JsonProcessingException, IOException {
		ReimbursementService reimbServiceCall = new ReimbursementServiceImpl();
		
		User user = (User) request.getSession().getAttribute("loggeduser");
		if(user == null)
			return;
		else if(user.getRole() != "finance manager")
			return;
		
		int reimbId = Integer.parseInt(request.getParameter("id"));
		System.out.println(reimbId);
		
		
		Reimbursement reimb = reimbServiceCall.getReimbursementById(reimbId);
		
		
		//Reimbursement reimb = (Reimbursement) request.getSession().getAttribute("reimb");
		//request.getSession().removeAttribute("reimb");
		response.getWriter().write(new ObjectMapper().writeValueAsString(reimb));
	}
	
	public static String updateReimbursement(HttpServletRequest request) {
		ReimbursementService reimbServiceCall = new ReimbursementServiceImpl();
		User user = (User) request.getSession().getAttribute("loggeduser");
		
		if(user == null)
			return "http://localhost:9002/ProjectOneReimbursements/";
		else if(user.getRole() != "finance manager")
			return "/resources/html/home.html";
		
		int reimbId = Integer.parseInt(request.getParameter("reimbId"));
		System.out.println(reimbId);
		
		Reimbursement reimb = reimbServiceCall.getReimbursementById(reimbId);
		
		
		reimb.setReimbResolver(user.getUserId());
		reimb.setReimbStatus(request.getParameter("reimbStatus").toLowerCase());
		reimb.setReimbType(request.getParameter("reimbType").toLowerCase());
		reimbServiceCall.updateReimbursement(reimb, user);
		return "/resources/html/home.html";
	}




}
