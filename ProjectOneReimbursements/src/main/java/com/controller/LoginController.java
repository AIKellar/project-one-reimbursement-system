package com.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.dao.ReimbursementDaoImpl;
import com.model.User;
import com.service.UserService;
import com.service.UserServiceImpl;

public class LoginController {
	
	final static Logger log = Logger.getLogger(LoginController.class);

	public static String login(HttpServletRequest request) {
		UserService userServiceCall = new UserServiceImpl();

		if(!request.getMethod().equals("POST")) {
			return "/index.html";
		}
		
		//extracting form data
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		
		User user = userServiceCall.checkCredentials(username, password);
		
		//check to see if the user has the correct username and password
		
			request.getSession().setAttribute("loggeduser", user);
			System.out.println(user);
			if(user != null) {
				log.info("User was logged in: " +user);
				return "/forwarding/home";
			}
			else 	
				return"/";
		
	}

	public static String logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return "/";
	}

}

