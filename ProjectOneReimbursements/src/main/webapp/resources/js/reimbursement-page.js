/**
 * 
 */
window.onload = function() {
	getCurrentReimb();
	getUsers();
	document.getElementById('logout').addEventListener("click",logout);
	
}
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
const id = urlParams.get('id')
let reimbursement = {};
let allUsers = {};
let user = {};
function getCurrentReimb() {
	fetch(
			`http://localhost:9002/ProjectOneReimbursements/json/reimbursement`+ '?id=' +id)
			.then(function(daResponse) {
				const convertedResponse = daResponse.json();
				console.log("converted response: ",convertedResponse);
				return convertedResponse;
			}).then(function(daSecondResponse) {
				console.log("dasecond response: ",daSecondResponse);
				reimbursement=daSecondResponse;
				console.log("reimbursement: ",reimbursement);
				let reimbursement1 = daSecondResponse;
				console.log("reimbursement1 :",reimbursement1);
				//setReimb(reimbursement1, user1);
				getUsers();
				
				
			})
}

function getUsers() {
	fetch(
			'http://localhost:9002/ProjectOneReimbursements/json/all-users')
			.then(function(daResponse) {
				const convertedResponse = daResponse.json();
				return convertedResponse;
			}).then(function(daSecondResponse) {
				console.log(daSecondResponse);
				allUsers = daSecondResponse;
				let allUsers1 =daSecondResponse;
				getUserForReimbursement(allUsers1);
				
			})
}

function getUserForReimbursement(allUsers1) {
console.log("allusers1: ",allUsers1);
	for(i=0; i<allUsers1.length; i++) {
		let user1;
		if(allUsers1[i].userId == reimbursement.reimbAuthor) {
			console.log("user id: ",allUsers1[i]);
			console.log("reimb author: ",reimbursement.reimbAuthor);
			console.log("hello");
			console.log("specific user: ",allUsers1[i]);
			user1 = allUsers1[i];
			console.log("user1: ",user1);
			setReimb(reimbursement, user1);
			}
	}
	
}

function setReimb(currReimbursement, user1) {
	let id = document.getElementById('id');
	id.setAttribute("value",currReimbursement.reimbId);

	let username = document.getElementById('username');
	username.setAttribute("value", user1.username);

	let amount = document.getElementById('amount');
	amount.setAttribute("value",currReimbursement.reimbAmount);
	
	let description = document.getElementById('desc');
	description.innerText = currReimbursement.reimbDescription;
	
	let status = currReimbursement.reimbStatus;
	switch(status) {
		case "pending" : {
			let pending = document.getElementById('pending');
			pending.setAttribute("selected", "selected");
			break;
		}
		case "denied" : {
			let denied = document.getElementById('denied');
			denied.setAttribute("selected", "selected");
			break;
		}
		case "approved" : {
			let approved = document.getElementById('approved');
			approved.setAttribute("selected", "selected");
			break;
		}
	}
	let type = currReimbursement.reimbType;
	switch(type) {
		case "LODGING" : {
			let lodging = document.getElementById('lodging');
			lodging.setAttribute("selected","selected");
			break;
		}
		case "TRAVEL"  : {
			let travel = document.getElementById('travel');
			travel.setAttribute("selected","selected");
			break;
		}
		case "FOOD"  : {
			let food = document.getElementById('food');
			food.setAttribute("selected","selected");
			break;
		}
		case "OTHER"  : {
			let other = document.getElementById('other');
			other.setAttribute("selected","selected");
			break;
		}
	}
	//let type = document.getElementById('type');
	//type.setAttribute("value",currReimbursement.reimbType);
	//type.readOnly = true;
}

function logout() {
	console.log("logout triggered");
	window.location = 'http://localhost:9002/ProjectOneReimbursements/forwarding/logout';
}


 