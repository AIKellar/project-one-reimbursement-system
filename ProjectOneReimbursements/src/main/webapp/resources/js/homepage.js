
window.onload = function() {
	window.history.pushState("", "Home", "/ProjectOneReimbursements/forwarding/home");
	getCurrentUser();
}


let users = {};
let myUserInformation = {};
let myReimbursements = {};
let myUserReimbursements = {};

function getCurrentUser() {
	fetch(
			'http://localhost:9002/ProjectOneReimbursements/json/user')
			.then(function(daResponse) {
				const convertedResponse = daResponse.json();
				return convertedResponse;
			}).then(function(daSecondResponse) {
				console.log(daSecondResponse);
				if(daSecondResponse == null)
					window.location = 'http://localhost:9002/ProjectOneReimbursements/forwarding/logout';
				myUserInformation=daSecondResponse;
				console.log("user logged in:" ,myUserInformation);
				if(myUserInformation.role == "employee")
					getUserReimbursements();
				else {
					getAllUsers();
					
					}

			})
}

function getAllUsers() {
	fetch(
			'http://localhost:9002/ProjectOneReimbursements/json/all-users')
			.then(function(daResponse) {
				const convertedResponse = daResponse.json();
				return convertedResponse;
			}).then(function(daSecondResponse) {
				console.log(daSecondResponse);
				users=daSecondResponse;
				getReimbursements();
			})
}

function getUserReimbursements() {
	fetch(
			'http://localhost:9002/ProjectOneReimbursements/json/userReimbursements')
			.then(function(daResponse) {
				const convertedResponse = daResponse.json();
				return convertedResponse;
			}).then(function(daSecondResponse) {
				console.log(daSecondResponse);
				myUserReimbursements=daSecondResponse;
				console.log("reimb amount:" + myUserReimbursements.reimbAmount)
				reimbDomManipulation(myUserReimbursements);
				navDomManipulation("employee");
			})
}

 function getReimbursements() {
	fetch(
			'http://localhost:9002/ProjectOneReimbursements/json/reimbursements')
			.then(function(daResponse) {
				const convertedResponse = daResponse.json();
				return convertedResponse;
			}).then(function(daSecondResponse) {
				console.log(daSecondResponse);
				myReimbursements=daSecondResponse;
				reimbDomManipulation(myReimbursements, "manager");
				navDomManipulation("manager");
			})
}

 function reimbDomManipulation(reimbursements, role) {
	if(role == "manager") {
		 let x = document.getElementById("filterButtons");
		 x.style.display = "block";
		 console.log(users);
		 x.classList.add('d-flex');
	}
	else {
		let x = document.getElementById("filterButtons");
		x.classList.remove('d-flex');
	}

	let tableBody = document.getElementById("reimbTableBody");
	for(i=0; i<reimbursements.length; i++) {
		let newTr = document.createElement("tr");
		let currentReimb = reimbursements[i];
		if(role == "manager") {
			newTr.addEventListener("click", function() {viewReimb(currentReimb)});
			newTr.setAttribute("style", " cursor: pointer;")
			}
		
		newTr.setAttribute("scope", "row");
		newTr.setAttribute("class", "all");
		let newTh = document.createElement("th");

		let newTd1 = document.createElement("td");
		newTd1.setAttribute("scope", "col");
		let newTd2 = document.createElement("td");
		newTd2.setAttribute("scope", "col");
		let newTd3 = document.createElement("td");
		newTd3.setAttribute("scope", "col");
		let newTd4 = document.createElement("td");
		newTd4.setAttribute("scope", "col");
		let newTd5 = document.createElement("td");
		newTd4.setAttribute("scope", "col");
		let newTd6 = document.createElement("td");
		newTd4.setAttribute("scope", "col");
		let newTd7 = document.createElement("td");
		newTd4.setAttribute("scope", "col");

		let num = document.createTextNode(reimbursements[i].reimbId);
		let username = "";
		if(role=="manager") {
			console.log("users array length: ", users.length);
			for(k=0; k<users.length; k++) {
				if(reimbursements[i].reimbAuthor == users[k].userId) {
					//username = document.createTextNode(users[k].username);
					newTd1.appendChild(document.createTextNode(users[k].username));
				}
			}
			console.log(username);
		}
		let amount = document.createTextNode("$"+reimbursements[i].reimbAmount);
		let submitted = document.createTextNode(reimbursements[i].reimbSubmitted.slice(0,16));
		let resolved = ""; 
		if(reimbursements[i].reimbResolved == null) {
			resolved = document.createTextNode("Not Resolved Yet");
		}
		else
			resolved = document.createTextNode(reimbursements[i].reimbResolved.slice(0,16));
		let desc = "";
		if(role == "manager") {
		if(reimbursements[i].reimbDescription.length > 50)
			desc = document.createTextNode(reimbursements[i].reimbDescription.slice(0,50)+"\n...");
		else
			desc =  document.createTextNode(reimbursements[i].reimbDescription);
			}
		else
			desc = document.createTextNode(reimbursements[i].reimbDescription);
		let status = document.createTextNode(reimbursements[i].reimbStatus);
		let type = document.createTextNode(reimbursements[i].reimbType);
		console.log(reimbursements[i].reimbResolved);
		if(reimbursements[i].reimbStatus == "pending") {
			newTr.classList.add("pending");
		}
		else if(reimbursements[i].reimbStatus == "denied") {
			newTr.classList.add("denied");
		}
		else
			newTr.classList.add("approved");
			console.log(username);
		if(role == "manager") {
			
			//newTd1.appendChild(document.createTextNode(users[k].username));
			}
		else
			newTd1.appendChild(num);
			
		newTd2.appendChild(amount);
		newTd3.appendChild(submitted);
		newTd4.appendChild(resolved);
		newTd5.appendChild(desc);
		newTd6.appendChild(status);
		newTd7.appendChild(type);

		newTr.appendChild(newTd1);
		newTr.appendChild(newTd2);
		newTr.appendChild(newTd3);
		newTr.appendChild(newTd4);
		newTr.appendChild(newTd5);
		newTr.appendChild(newTd6);
		newTr.appendChild(newTd7);

		tableBody.appendChild(newTr);
		


	}
}

function navDomManipulation(role) {
	console.log("in nav dom function");
	if(role == "employee") {
		console.log("in employee nav dom");
		let navLinks = document.getElementById("navLinks");
		let listItem1 = document.createElement("li");
		listItem1.setAttribute("class", "nav-item");

		let navLink1 = document.createElement("a");
		navLink1.setAttribute("class", "nav-link");
		navLink1.setAttribute("href", "/ProjectOneReimbursements/forwarding/create-reimbursement");
		let text = document.createTextNode("New Reimbursement");
		navLink1.appendChild(text);
		listItem1.appendChild(navLink1);
		navLinks.appendChild(listItem1);
	} else {

	}
}

function filter(type) {
	let allTypes = document.getElementsByClassName("all");
	if(type == "all") {
		for(let i=0; i<allTypes.length; i++) {
			allTypes[i].setAttribute("style", "display: ");
		}
	}
	else if(type == "pending") {
		for(let i=0; i<allTypes.length; i++) {
			if(allTypes[i].classList.contains("approved") || allTypes[i].classList.contains("denied")) {
				allTypes[i].setAttribute("style", "display: none");
			}
			else
				allTypes[i].setAttribute("style", "display: ");
		}
	}
	else if(type == "denied") {
		for(let i=0; i<allTypes.length; i++) {
			if(allTypes[i].classList.contains("approved") || allTypes[i].classList.contains("pending")) {
				allTypes[i].setAttribute("style", "display: none");
			}
			else
				allTypes[i].setAttribute("style", "display: ");
		}
	}
	else if(type == "approved") {
		for(let i=0; i<allTypes.length; i++) {
			if(allTypes[i].classList.contains("denied") || allTypes[i].classList.contains("pending")) {
				allTypes[i].setAttribute("style", "display: none");
			}
			else
				allTypes[i].setAttribute("style", "display: ");
		}
	}

}


function viewReimb(reimbursement) {
	window.location.href = 'http://localhost:9002/ProjectOneReimbursements/forwarding/reimbursement' + '?id='+reimbursement.reimbId;
}

