# Project One Reimbursement System

## Project Description

The Expense Reimbursement System (ERS) manages the process of reimbursing employees for expenses incurred while on company time. All employees can log in and submit requests for reimbursement and view their past tickets and pending requests. Managers can then log in and view all reimbursement requests and past history for all employees. These managers are authorized to approve and deny requests for expense reimbursements.

## Technologies Used

* Java 8
* HTML, CSS, JavaScript
* Log4J
* JUnit
* AJAX
* JDBC
* Servlets

## Features

List of features ready and TODOs for future development
* Dynamic dom manipulation with AJAX to display interactice views
* Proper route guarding to prevent unauthorized access to back-end api's
* Full user authentication with custom backend 

To-do list:
* Refactor Back-end to implement Spring ORM and MVC
* Refactor Front-end to implement React

## Getting Started

> git clone https://gitlab.com/AIKellar/project-two-social-media.git

- import the backend project into maven and add apache tomcat to your targeted runtimes
- add this project to your tomcat server. To download Tomcat go here: http://tomcat.apache.org/
- Run your Tomcat server
- type http://localhost:[your port number]
- You should be good to go.

## Usage

> ![](https://i.imgur.com/FYI1SSB.png)
> ![](https://i.imgur.com/TpRMppT.png)

## Contributors

> @AIKellar
